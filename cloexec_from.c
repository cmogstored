/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "cmogstored.h"

/*
 * this function is only called in a vforked child (for iostat)
 * if O_CLOEXEC/SOCK_CLOEXEC is unsupported, or if mog_cloexec_detect()
 * detects those flags are broken.
 */
void mog_cloexec_from(int lowfd) /* vfork-safe */
{
	int fd;
	int last_good = lowfd;

	for (fd = lowfd; fd < INT_MAX; fd++) {
		if (mog_set_cloexec(fd, true) == 0)
			last_good = fd;
		if ((last_good + 1024) < fd)
			break;
	}
}
