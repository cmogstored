/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
/*
 * process management for iostat(1)
 * Since iostat(1) watches the entire system, we only spawn it once
 * regardless of the number of mog_svc objects we have.
 */
#include "cmogstored.h"

static pid_t iostat_pid;
static time_t iostat_last_fail;
static struct mog_iostat *iostat;
static time_t iostat_fail_timeout = 10;

enum mog_exerr {
	MOG_EXERR_DUP2 = 3,
	MOG_EXERR_SIGPROCMASK = 4,
	MOG_EXERR_EXECVE = 5
};

static void iostat_atexit(void)
{
	if (iostat_pid > 0)
		kill(iostat_pid, SIGTERM);
}

static int iostat_pipe_init(int *fds)
{
	if (pipe2(fds, O_CLOEXEC) < 0) {
		PRESERVE_ERRNO( syslog(LOG_ERR, "pipe2() failed: %m") );

		/*
		 * don't retry here, MFS can deal with not getting iostat
		 * data for a while
		 */
		if (errno == ENFILE || errno == EMFILE)
			PRESERVE_ERRNO( (void)mog_fdmap_expire(5) );
		return -1;
	}

	CHECK(int, 0, mog_set_nonblocking(fds[0], true));
	/* fds[1] (write end) stays _blocking_ */

	return 0;
}

static const char *exec_cmd(const char *cmd)
{
	time_t last_fail = time(NULL) - iostat_last_fail;
	time_t delay = iostat_fail_timeout - last_fail;

	if (delay <= 0)
		return xasprintf("exec %s", cmd);

	syslog(LOG_DEBUG,
	       "delaying exec of `%s' for %ds due to previous failure",
	       cmd, (int)delay);
	return xasprintf("sleep %d; exec %s", (int)delay, cmd);
}

static int dup2_retry(int oldfd, int newfd) /* vfork-safe */
{
	int rc;

	do
		rc = dup2(oldfd, newfd);
	while (rc < 0 && (errno == EINTR || errno == EBUSY));

	return rc;
}

static void execve_iostat(int out_fd, const char *cmd) /* vfork-safe */
{
	int i;
	union {
		char *argv[4];
		char const *in[4];
	} u;

	u.in[0] = "/bin/sh";
	u.in[1] = "-c";
	u.in[2] = cmd;
	u.in[3] = 0;

	if (dup2_retry(out_fd, STDOUT_FILENO) < 0)
		_exit(MOG_EXERR_DUP2);
	if (!mog_cloexec_atomic)
		mog_cloexec_from(STDERR_FILENO + 1);

	/* ignore errors, not much we can do about missing signals */
	for (i = 1; i < NSIG; i++)
		(void)signal(i, SIG_DFL);

	if (sigprocmask(SIG_SETMASK, &mog_emptyset, NULL) != 0)
		_exit(MOG_EXERR_SIGPROCMASK);

	execve("/bin/sh", u.argv, environ);
	_exit(MOG_EXERR_EXECVE);
}

static pid_t iostat_fork_exec(int out_fd)
{
	/* rely on /bin/sh to parse iostat command-line args */
	const char *cmd = getenv("MOG_IOSTAT_CMD");
	int cs;

	if (!cmd)
		cmd = "iostat -dx 1 30";

	cmd = exec_cmd(cmd);
	CHECK(int, 0, pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cs));
	iostat_pid = mog_fork_for_exec();
	if (iostat_pid < 0)
		syslog(LOG_ERR, "fork() for iostat failed: %m");
	else if (iostat_pid == 0) /* child */
		execve_iostat(out_fd, cmd);

	/* parent */
	CHECK(int, 0, pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, 0));
	if (iostat_pid > 0)
		mog_process_register(iostat_pid, MOG_PROC_IOSTAT);
	mog_close(out_fd);
	mog_free(cmd);

	return iostat_pid;
}

bool mog_iostat_respawn(int oldstatus)
{
	int fds[2];
	struct mog_fd *mfd;

	if (WIFEXITED(oldstatus)) {
		int ex = WEXITSTATUS(oldstatus);
		const char *fn = 0;

		switch (ex) {
		case 0: break;
		case MOG_EXERR_DUP2: fn = "dup2"; break;
		case MOG_EXERR_SIGPROCMASK: fn = "sigprocmask"; break;
		case MOG_EXERR_EXECVE: fn = "execve"; break;
		default: fn = "(unknown)";
		}
		if (fn)
			syslog(LOG_ERR, "iostat exited due to %s failure", fn);
		/* else syslog(LOG_DEBUG, "iostat done, restarting"); */
	} else {
		iostat_last_fail = time(NULL);
		syslog(LOG_WARNING,
		       "iostat done (pid=%d, status=%d), will retry in %ds",
		       (int)iostat_pid, oldstatus, (int)iostat_fail_timeout);
	}
	iostat_pid = 0;

	if (iostat_pipe_init(fds) < 0)
		return false; /* EMFILE || ENFILE */
	if (iostat_fork_exec(fds[1]) < 0)
		return false; /* fork() failure */

	assert(fds[0] >= 0 && "invalid FD");

	mfd = mog_fd_init(fds[0], MOG_FD_TYPE_IOSTAT);

	if (iostat == NULL)
		atexit(iostat_atexit);
	iostat = &mfd->as.iostat;
	iostat->queue = mog_notify_queue;
	mog_iostat_init(iostat);
	mog_idleq_add(iostat->queue, mfd, MOG_QEV_RD);

	return true;
}
