/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */

static inline void mog_activeq_add(struct mog_queue *q, struct mog_fd *mfd)
{
	mog_idleq_add(q, mfd, MOG_QEV_RW);
}

static inline void mog_activeq_push(struct mog_queue *q, struct mog_fd *mfd)
{
	mog_idleq_push(q, mfd, MOG_QEV_RW);
}
