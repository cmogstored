/*
 * note: probe names are upper-case in the source and prefixed by provider
 * So we'll have trace points like:
 *  CMOGSTORED_HTTP_RDERR for the "http_rderr" trace point
 *
 * Warning: probe points are currently an unstable interface and likely
 * to change in 2013
 */
provider cmogstored {
	probe http_rderr(int fd, size_t buf_len, int err);
	probe http_rdclose(int fd, size_t buf_len);
	probe http_accepted(int fd, const char *host, const char *port,
				const char *listen_addr);

	probe http_req_begin(int fd, bool pipelined);

	probe http_parse_continue(int fd, size_t buf_len);
	probe http_rbuf_grow(int fd, size_t buf_len);
	probe http_req_start(int fd, const char *method, const char *path);
	probe http_res_start(int fd, const char *status);
	probe http_res_done(int fd);
	probe http_client_close(int fd, size_t buf_len);
	probe http_bytes_xfer(int fd, off_t len);

	probe ioq_blocked(int fd);
	probe ioq_reschedule(int fd);
	probe ioq_unblocked(int fd);

	probe mgmt_accepted(int fd, const char *host, const char *port,
				const char *listen_addr);
	probe mgmt_dig_start(int fd, int alg, const char *path);
	probe mgmt_dig_done(int fd, long long code);
	probe mgmt_parse_continue(int fd, size_t buf_len);
	probe mgmt_rbuf_grow(int fd, size_t buf_len);
	probe mgmt_rderr(int fd, size_t buf_len, int err);
	probe mgmt_client_close(int fd, size_t buf_len);

	probe write_buffered(int fd, size_t buf_len);
};
