/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#if defined(HAVE_EPOLL_WAIT) && ! MOG_LIBKQUEUE
#include <sys/epoll.h>

/*
 * EPOLLERR and EPOLLHUP are always set by default,
 * but there's no harm in setting them here
 */
#define MY_EP_FLAGS (EPOLLONESHOT|EPOLLHUP|EPOLLERR)
enum mog_qev {
	MOG_QEV_RD = EPOLLIN | MY_EP_FLAGS | EPOLLET,
	MOG_QEV_WR = EPOLLOUT | MY_EP_FLAGS | EPOLLET,
	MOG_QEV_RW = EPOLLIN | EPOLLOUT | MY_EP_FLAGS
};
#endif /* HAVE_EPOLL_WAIT */
