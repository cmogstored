/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "cmogstored.h"

static int stat_prefix(struct mog_svc *svc, struct stat *sb, uint32_t mog_devid)
{
	char prefix[sizeof("/dev" MOG_STR(MOG_DEVID_MAX) "/")];
	int rc;

	assert(mog_devid <= MOG_DEVID_MAX && "mog_devid not filtered");
	rc = snprintf(prefix, sizeof(prefix), "/dev%u/", mog_devid);
	assert(rc > 0 && rc < (int)sizeof(prefix) && "we suck at snprintf");

	return mog_stat(svc, prefix, sb);
}

static struct mog_dev *mog_dev_new(struct mog_svc *svc, uint32_t mog_devid)
{
	struct mog_dev *dev;
	struct stat sb;

	/* we have no space on stack */
	if (mog_devid > MOG_DEVID_MAX)
		return NULL;

	if (stat_prefix(svc, &sb, mog_devid) < 0)
		return NULL;

	dev = mog_cachealign(sizeof(struct mog_dev));
	dev->devid = mog_devid;
	dev->st_dev = sb.st_dev;
	mog_ioq_init(&dev->fsckq, svc, 1);
	mog_ioq_init(&dev->ioq, svc, svc->thr_per_dev);
	dev->no_me_warned = 0;
	dev->usage_txt = 0;
	dev->usage_len = 0;
	dev->usage_mtime = 0;
	CHECK(int, 0, pthread_mutex_init(&dev->usage_lock, NULL));

	return dev;
}

struct mog_dev *
mog_dev_for(struct mog_svc *svc, uint32_t mog_devid, bool update)
{
	struct mog_dev finder;
	struct mog_dev *ret;
	bool need_refresh = false;

	finder.devid = mog_devid;

	CHECK(int, 0, pthread_mutex_lock(&svc->by_mog_devid_lock));
	ret = hash_lookup(svc->by_mog_devid, &finder);
	if (ret) {
		struct stat sb;

		if (!update)
			goto out;

		/*
		 * devXXX dir existed before, but is no longer readable
		 * Possible FS/device error, it could come back, so do
		 * not remove here.
		 */
		if (stat_prefix(svc, &sb, ret->devid) < 0)
			goto out;

		/* st_dev may change due to remount, update if needed */
		ret->st_dev = sb.st_dev;
	} else { /* create a new dev */
		ret = mog_dev_new(svc, mog_devid);

		if (!ret)
			goto out; /* could not stat */

		switch (hash_insert_if_absent(svc->by_mog_devid, ret, NULL)) {
		case 0:
			assert(0 && "mog_dev existed while adding");
			abort();
		case 1:
			if (!update)
				need_refresh = true;
			break; /* OK, inserted */
		default: mog_oom();
		}
	}
out:
	CHECK(int, 0, pthread_mutex_unlock(&svc->by_mog_devid_lock));

	/* we need to get the notify thread to create new worker threads */
	if (need_refresh)
		mog_notify(MOG_NOTIFY_DEVICE_REFRESH);

	return ret;
}


size_t mog_dev_hash(const void *x, size_t tablesize)
{
	const struct mog_dev *dev = x;

	return dev->devid % tablesize;
}

bool mog_dev_cmp(const void *a, const void *b)
{
	const struct mog_dev *dev_a = a;
	const struct mog_dev *dev_b = b;

	return dev_a->devid == dev_b->devid;
}

static int
emit_usage(struct mog_dev *dev, struct mog_svc *svc, int fd, struct statvfs *v)
{
	int rc = -1;
	unsigned long long available = v->f_bavail;
	unsigned long long total = v->f_blocks - (v->f_bfree - v->f_bavail);
	unsigned long long used = v->f_blocks - v->f_bfree;
	unsigned use = (used * 100) / total + !!((used * 100) % total);
	long long now = (long long)time(NULL);
	long double mb = v->f_frsize / (long double)1024.0;
	const struct mount_entry *me;
	char *usage_txt;
	static const char usage_fmt[] =
		"available: %llu\n"
		"device: %s\n"
		"disk: %s/dev%u\n"
		"time: %lld\n"
		"total: %llu\n"
		"use: %u%%\n"
		"used: %llu\n";

	available *= mb;
	total *= mb;
	used *= mb;

	if (use > 100)
		use = 100;

	me = mog_mnt_acquire(dev->st_dev);
	if (!me && !dev->no_me_warned) {
		syslog(LOG_ERR, "mount entry not found for %s/dev%u",
		       svc->docroot, (unsigned)dev->devid);
		dev->no_me_warned = 1;
	}
	errno = 0;
	rc = asprintf(&usage_txt, usage_fmt, available,
			/* device:, MogileFS::Worker::Monitor doesn't care: */
			me ? me->me_devname : "(?)",
			svc->docroot,
			(unsigned)dev->devid, now, total, use, used);
	if (rc > 0) {
		char *old_usage;

		CHECK(int, 0, pthread_mutex_lock(&dev->usage_lock));
		old_usage = dev->usage_txt;
		dev->usage_txt = usage_txt;
		dev->usage_len = rc;
		dev->usage_mtime = (time_t)now;
		CHECK(int, 0, pthread_mutex_unlock(&dev->usage_lock));

		free(old_usage);
		if (fd >= 0) {
			ssize_t w = write(fd, usage_txt, rc);

			if (w >= 0 && w != rc)
				errno = ENOSPC;
			else if (w < 0)
				rc = -1;
		}
	}
	if (me)
		PRESERVE_ERRNO( mog_mnt_release(me) );
	if (rc < 0 || errno == ENOSPC) {
		PRESERVE_ERRNO(do {
			syslog(LOG_ERR, "write(%s/dev%u/usage): %m",
			       svc->docroot, (unsigned)dev->devid);
		} while (0));
	}

	return rc;
}

static void
dev_usage_update(struct mog_dev *dev, struct mog_svc *svc, struct statvfs *v)
{
	if (mog_statvfs(svc, dev, v) < 0) {
		syslog(LOG_ERR, "statvfs error: %s/dev%u/usage (%m)",
			svc->docroot, (unsigned)dev->devid);
		return;
	}
	(void)emit_usage(dev, svc, -1, v);
}

void
mog_dev_usage_update(struct mog_dev *dev, struct mog_svc *svc)
{
	struct statvfs v;

	dev_usage_update(dev, svc, &v);
}

int mog_dev_mkusage(struct mog_dev *dev, struct mog_svc *svc)
{
	struct statvfs v;
	char *usage_path;
	char *tmp_path;
	int fd = -1;
	struct stat sb;

	if (!svc->mgmt_mfd)
		return 0;

	if (asprintf(&usage_path, "/dev%u/usage", (unsigned)dev->devid) < 0) {
		syslog(LOG_ERR, "error generating path: /dev%u/usage (%m)",
			(unsigned)dev->devid);
		return 0;
	}

	/*
	 * allow chmod 0000 on devNNN/usage files to prevent us from
	 * overwriting them
	 */
	if (mog_stat(svc, usage_path, &sb) == 0 &&
	    ((sb.st_mode & (S_IRWXU|S_IRWXG|S_IRWXO)) == 0)) {
		dev_usage_update(dev, svc, &v);
		tmp_path = 0;
		goto out;
	}
	if (asprintf(&tmp_path, "%s.%x", usage_path, (unsigned)getpid()) < 0) {
		syslog(LOG_ERR, "error generating path: /dev%u/usage.%u (%m)",
			(unsigned)dev->devid, (unsigned)getpid());
		free(usage_path);
		return 0;
	}

	if (mog_unlink(svc, tmp_path) < 0 && errno != ENOENT) goto out;

	errno = 0;
	fd = mog_open_put(svc, tmp_path, O_EXCL|O_CREAT);
	if (fd < 0) {
		PRESERVE_ERRNO(do {
			syslog(LOG_ERR, "open(%s%s): %m",
			       svc->docroot, tmp_path);
		} while (0));
		dev_usage_update(dev, svc, &v);
		goto out;
	}
	if (fstatvfs(fd, &v) < 0) {
		PRESERVE_ERRNO(do {
			syslog(LOG_ERR, "fstatvfs(%s%s): %m",
			       svc->docroot, tmp_path);
		} while (0));
		goto out;
	}
	if (emit_usage(dev, svc, fd, &v) < 0) goto out;
	if (fchmod(fd, svc->put_perms) < 0) {
		PRESERVE_ERRNO(do {
			syslog(LOG_ERR, "fchmod(%s%s): %m",
			       svc->docroot, tmp_path);
		} while (0));
		goto out;
	}

	/* skip rename on EIO if close() fails */
	if (close(fd) != 0) {
		assert(errno != EBADF && "attempted to close bad FD");
		fd = -1;
		if (errno != EINTR) {
			/* possible EIO */
			PRESERVE_ERRNO(do {
				syslog(LOG_ERR, "close(%s%s) failed: %m",
			               svc->docroot, tmp_path);
			} while (0));
			goto out;
		}
	}

	fd = -1;
	errno = 0;
	if (mog_rename(svc, tmp_path, usage_path) != 0) {
		PRESERVE_ERRNO(do {
			syslog(LOG_ERR, "rename(%s(%s => %s))",
			       svc->docroot, tmp_path, usage_path);
		} while (0));
	}
out:
	PRESERVE_ERRNO(do {
		if (errno)
			(void)mog_unlink(svc, tmp_path);
		if (fd >= 0)
			(void)mog_close(fd);
		free(tmp_path);
		free(usage_path);
	} while (0));
	return errno ? -1 : 0;
}

void mog_dev_free(void *ptr)
{
	struct mog_dev *dev = ptr;

	mog_ioq_destroy(&dev->fsckq);
	mog_ioq_destroy(&dev->ioq);
	free(dev->usage_txt);
	CHECK(int, 0, pthread_mutex_destroy(&dev->usage_lock));
	free(dev);
}

/*
 * Only called by the main/notify thread as a hash iterator function
 * This increases or decreases the capacity of a given device if a
 * a sidechannel user changes the worker thread pool size.
 */
bool mog_dev_user_rescale_i(void *devp, void *svcp)
{
	struct mog_dev *dev = devp;
	struct mog_svc *svc = svcp;

	mog_ioq_adjust(&dev->ioq, svc->thr_per_dev);

	return true; /* continue iteration */
}

bool mog_dev_requeue_prepare(void *devp, void *ign)
{
	struct mog_dev *dev = devp;

	mog_ioq_requeue_prepare(&dev->ioq);
	mog_ioq_requeue_prepare(&dev->fsckq);

	return true; /* continue iteration */
}
