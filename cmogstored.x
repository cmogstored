[SIGNALS]
SIGQUIT - gracefully shutdown the server

SIGUSR2 - upgrade executable on the fly

[UPGRADING]
Upgrading cmogstored on-the-fly is possible since version 1.2.0.
The procedure is close to the one used by nginx users: send SIGUSR2
followed by SIGQUIT to the old process.  Use of SIGWINCH and SIGHUP
are currently not supported (but may be in the future).
Procedures for the nginx upgrade are documented here:
https://www.nginx.com/resources/wiki/start/topics/tutorials/commandline/#upgrading-to-a-new-binary-on-the-fly

[ENVIRONMENT]
MOG_IOSTAT_CMD - command-line for invoking iostat(1).  This is used
by the sidechannel to report disk utilization to trackers.
Default: "iostat -dx 1 30"

LISTEN_FDS, LISTEN_PID - may be used for systemd-style socket activation
regardless of whether systemd is on the system.  See sd_listen_fds(3)
for more information.  Since cmogstored supports two protocols on
different sockets, the command line or config file must still specify
which addresses to listen on.  This feature is available in cmogstored
1.5.0 and later.

CMOGSTORED_FD - a comma-delimited list of integer file descriptors.
This is used internally for SIGUSR2 upgrades, but may be specified
at startup (e.g. via env(1)) as it is not tied to LISTEN_PID
like LISTEN_FDS is.  Unlike LISTEN_FDS, CMOGSTORED_FD does not rely
on inherited file descriptors being contiguous.

See MALLOC TUNING for environment variables which may affect memory
usage.

[USAGE FILES]

Like Perl mogstored(1), cmogstored writes "devNNN/usage" files for
mogilefsd(1) instances to monitor disk space utilization.

As of cmogstored 1.7, this information is stored in-memory in case the
device becomes too full to write the usage file.  Users may choose to
make usage files unwritable (via "chmod 000") to reduce disk wear on
otherwise read-mostly devices.

As of cmogstored 1.8, the in-memory representation of these usage files
is updated by PUT and DELETE HTTP requests in case write traffic is too
high for periodic (10s) updates.

[MALLOC TUNING]

malloc(3) implementations tuned for multithreading speed (and not memory
usage) may cause large amounts of metadata overhead in infrequently-used
arenas.  While cmogstored may use many threads for filesystem I/O,
cmogstored is designed to not be dependent on malloc(3) performance nor
scalability.

On GNU/Linux systems with glibc, we recommend setting MALLOC_ARENA_TEST
and MALLOC_ARENA_MAX to low numbers to limit the number arenas.  Setting
the value of each to 1 or 2 should be enough, even.

[REPORTING BUGS]
Only bug reports in plain-text email will be read.

Please email to either the public inbox at (no subscription required):

	<cmogstored-public@yhbt.net>

(archived at <https://yhbt.net/cmogstored-public/>)

or the public MogileFS mailing list (subscription required)

	<mogile@googlegroups.com>

(archived at <https://groups.google.com/forum/#!forum/mogile>).

[COPYRIGHT]
Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>

License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

[SEE ALSO]
cmogstored website: https://yhbt.net/cmogstored/README
You can learn more about MogileFS at https://github.com/mogilefs/MogileFS-Server

cmogstored source code is available via git:

	git clone https://yhbt.net/cmogstored.git
