/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
static void iov_str(struct iovec *iov, const char *str, size_t len)
{
	union { const char *in; char *out; } deconst;

	deconst.in = str;
	iov->iov_base = deconst.out;
	iov->iov_len = len;
}

#define IOV_STR(iov,str) iov_str((iov),(str),sizeof(str)-1)
