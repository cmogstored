# This makefile snippet is for maintainers only and _not_ distributed with
# the tarball.
# You should find this in the git repo (https://yhbt.net/cmogstored.git)
$(dist_man_MANS): $(bin_PROGRAMS)
