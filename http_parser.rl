/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "cmogstored.h"
#include "http_util.h"

static bool length_incr(off_t *len, unsigned c)
{
	off_t prev = *len;

	*len *= 10;
	*len += c - '0';

	if (*len >= prev)
		return true;

	errno = ERANGE;
	*len = -1;

	return false;
}

static char *skip_header(struct mog_http *http, char *buf, const char *pe)
{
	char *p;

	assert(http->_p.line_end > 0 && "no previous request/header line");
	assert(buf[http->_p.line_end] == '\n' && "bad http->_p.line_end");
	p = buf + http->_p.line_end + 1;
	assert(p <= pe && "overflow");

	return p;
}

%%{
	machine http_parser;
	include http_common "http_common.rl";
	include path_parser "path_parser.rl";

	ignored_header := header_name ':' sep header_value eor @ {
		fgoto more_headers;
	};

	GET = "GET "> { http->_p.http_method = MOG_HTTP_METHOD_GET; };
	HEAD = "HEAD "> { http->_p.http_method = MOG_HTTP_METHOD_HEAD; };
	PUT = "PUT "> { http->_p.http_method = MOG_HTTP_METHOD_PUT; };
	DELETE = "DELETE "> { http->_p.http_method = MOG_HTTP_METHOD_DELETE; };
	MKCOL = "MKCOL "> { http->_p.http_method = MOG_HTTP_METHOD_MKCOL; };

	mog_path_start = '/' > { http->_p.path_tip = to_u8(fpc - buf); };
		# TODO: maybe folks use query string/fragments for logging...
	mog_path_end = (" HTTP/1.") > { http->_p.path_end = to_u8(fpc - buf); };
	usage_path = ("usage HTTP/1.") @ { http->_p.usage_txt = 1; };

	# no HTTP/0.9 for now, sorry (not :P)
	req_line = (HEAD|GET|PUT|DELETE|MKCOL)
		("http://" [^/]+)?
		'/'* mog_path_start devid? (usage_path |
					    (mog_path_rest mog_path_end) )
		('0'|'1'> { http->_p.persistent = 1; }) '\r'LF;

	content_length = "Content-Length:"i sep
		(digit+) $ {
			/*
			 * RFC 7230 3.3.2, 3.3.3,:
			 * favor Transfer-Encoding over Content-Length
			 */
			if (!http->_p.chunked &&
					!length_incr(&http->_p.content_len, fc))
				fbreak;
		}
		$! { errno = EINVAL; fbreak; }
		eor;
	content_range = "Content-Range:"i sep "bytes"LWS+
		(digit+) $ {
			if (!length_incr(&http->_p.range_beg, fc))
				fbreak;
		}
		$! { errno = EINVAL; fbreak; }
		"-"
		(digit+) $ {
			if (!length_incr(&http->_p.range_end, fc))
				fbreak;
		}
		$! { errno = EINVAL; fbreak; }
		"/*"
		eor > { http->_p.has_content_range = 1; };
	range = "Range:"i sep (
			"bytes=" > {
				http->_p.range_beg = http->_p.range_end = -1;
			}
			(
				(digit*) $ {
					if (http->_p.range_beg < 0)
						http->_p.range_beg = 0;
					if (!length_incr(&http->_p.range_beg,
							 fc))
						fbreak;
				}
				'-'
				(digit*) $ {
					if (http->_p.range_end < 0)
						http->_p.range_end = 0;
					if (!length_incr(&http->_p.range_end,
							 fc))
						fbreak;
				}
			) $! {
				http->_p.bad_range = 1;
				p = skip_header(http, buf, pe);
				fgoto ignored_header;
			}
		) $! {
			p = skip_header(http, buf, pe);
			fgoto ignored_header;
		}
		eor @ { http->_p.has_range = 1; };
	transfer_encoding_chunked = "Transfer-Encoding:"i sep
		# XXX we don't know how to deal with "gzip", "deflate", or
		# "compress" as described in RFC 7230, so reject them, here.
		"chunked"i
		$! { errno = EINVAL; fbreak; }
		eor @ {
			http->_p.chunked = 1;
			/* RFC 7230 3.3.2, 3.3.3,: ignore length if chunked */
			http->_p.content_len = 0;
		};
	trailer = "Trailer:"i sep
		(("Content-MD5"i @ { http->_p.has_md5 = 1; })
		 | header_name | ',')
		eor;
	connection = "Connection:"i sep
		(("close"i @ { http->_p.persistent = 0; }) |
		 ("keep-alive"i @ { http->_p.persistent = 1; })) eor;
	header_line =
		( content_length |
		  transfer_encoding_chunked |
		  trailer |
		  range |
		  content_range |
		  content_md5 |
		  connection ) $!
		{
			p = skip_header(http, buf, pe);
			fgoto ignored_header;
		};
	headers = header_line* '\r''\n' > { really_done = 1; fbreak; };
	more_headers := headers;
	main := req_line headers;
}%%

%% write data;

void mog_http_reset_parser(struct mog_http *http)
{
	int cs;

	%% write init;
	http->cs = cs;
	memset(&http->_p, 0, sizeof(http->_p));

	if (http->rbuf) /* already pipelined */
		http->_p.persist_client_at_start = 1;
	else
		/*
		 * we need to know persist_client when we start reading
		 * the request because we do not want to break pipelined
		 * requests
		 */
		http->_p.persist_client_at_start = http->svc->persist_client;

	/* these should probably be in mog_http_init */
	http->forward = NULL;
	http->wbuf = NULL;
}

void mog_http_init(struct mog_http *http, struct mog_svc *svc)
{
	http->svc = svc;
	http->rbuf = NULL;
	mog_http_reset_parser(http);
}

enum mog_parser_state
mog_http_parse(struct mog_http *http, char *buf, size_t len)
{
	char *p, *pe, *eof = NULL;
	int cs = http->cs;
	int really_done = 0;
	size_t off = http->_p.buf_off;
	uint32_t *mog_devid = &http->_p.mog_devid;

	assert(http->wbuf == NULL && "unwritten data in buffer");
	assert(off <= len && "http offset past end of buffer");

	p = buf + off;
	pe = buf + len;

	assert((void *)(pe - p) == (void *)(len - off) &&
	       "pointers aren't same distance");

	errno = 0;
	%% write exec;

	if (really_done)
		cs = http_parser_first_final;

	http->cs = cs;
	http->_p.buf_off = p - buf;

	if (cs == http_parser_error || errno)
		return MOG_PARSER_ERROR;

	assert(p <= pe && "buffer overflow after http parse");
	assert(http->_p.buf_off <= len && "offset longer than len");

	if (http->cs == http_parser_first_final) {
		http->_p.persistent &= http->_p.persist_client_at_start;
		return MOG_PARSER_DONE;
	}
	return MOG_PARSER_CONTINUE;
}
