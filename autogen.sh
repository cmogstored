#!/bin/sh
>> test/slow.mk
glver="$(gnulib-tool --version | head -n1)"
if gnulib-tool --update && autoreconf -i
then
	echo "$glver" > .gnulib-version
	if ! grep -w free_mount_entry >/dev/null lib/mountlist.h
	then
		cat >&2 <<EOF

ERROR: gnulib too old
gnulib commit 41d1b6c42641a5b9e21486ca2074198ee7909bd7
("mountlist: add support for deallocating returned list entries")
or later (from July 2013) for free_mount_entry support

EOF
		exit 1
	fi
	exit 0
fi
cat HACKING
exit 1
