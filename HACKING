Bootstrapping from the git repo requires GNU autotools, Gnulib and
Ragel.  You will need "autoreconf", "gnulib-tool", "help2man" and
"ragel" in your $PATH.  Most tests require "ruby" (1.8.7-p299 or later).
GNU make ("gmake" on non-GNU systems) is not required for normal
user builds and installation, but highly recommended for hackers.

* autoconf - https://www.gnu.org/software/autoconf/
* automake - https://www.gnu.org/software/automake/
* Gnulib - https://www.gnu.org/software/gnulib/
* help2man - https://www.gnu.org/software/help2man/
* Ragel - https://www.colm.net/open-source/ragel/ (parser)
* GNU make - https://www.gnu.org/software/make/
* ruby - https://www.ruby-lang.org/en/
* git - https://www.git-scm.com/

$ git clone https://yhbt.net/cmogstored.git
$ cd cmogstored && ./autogen.sh

Generally, the versions of these tools bundled with the latest
stable release of Debian GNU/Linux will work.

For Gnulib, we will use the latest git checkouts from:

	https://git.savannah.gnu.org/git/gnulib.git

We currently require gnulib commit 41d1b6c42641a5b9e21486ca2074198ee7909bd7
("mountlist: add support for deallocating returned list entries")
or later (from July 2013) for free_mount_entry support

For gcov code coverage reports, we rely on "gcov2perl" and "cover"
from the Devel::Cover Perl module:

* Devel::Cover - https://metacpan.org/release/Devel-Cover
  Debian users may install the "libdevel-cover-perl" package.

Additional development tools we use:

* valgrind - http://valgrind.org/
* sparse - https://sparse.wiki.kernel.org/

We also use Rake for pre-release checks, generating ChangeLog and NEWS
documentation, and the "builder" RubyGem for generating the Atom
feed for our website.

Hack away!

Email patches (git format-patch + git send-email) and pull requests to
the public list at <cmogstored-public@yhbt.net>
Subscription is optional (so please reply-to-all), but you may
subscribe by sending an email to <cmogstored-public+subscribe@yhbt.net>
and answering the confirmation.

HTML archives are available at https://yhbt.net/cmogstored-public/
and via NNTP(S) and IMAP(S):

  nntps://news.public-inbox.org/inbox.comp.file-systems.mogilefs.cmogstored
  imaps://news.public-inbox.org/inbox.comp.file-systems.mogilefs.cmogstored.0

Tor users may also use .onions:

  nntp://ou63pmih66umazou.onion/inbox.comp.file-systems.mogilefs.cmogstored
  imap://ou63pmih66umazou.onion/inbox.comp.file-systems.mogilefs.cmogstored.0
