/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */

union mog_sockaddr {
	struct sockaddr_in in;
	struct sockaddr_in6 in6;
	struct sockaddr sa;
	uint8_t bytes[sizeof(struct sockaddr_in6)];
};

/* this is the relevant part we may store in "struct mog_fd" */
struct mog_packaddr {
	sa_family_t sa_family;
	in_port_t port;
	union {
		struct in6_addr *in6_ptr;
		struct in_addr in_addr;
	} as;
} __attribute__((packed));

static inline void mog_packaddr_free(struct mog_packaddr *mpa)
{
	if (mpa->sa_family != AF_INET)
		free(mpa->as.in6_ptr);
}

static inline void mog_packaddr_init(struct mog_packaddr *mpa,
				union mog_sockaddr *msa, socklen_t salen)
{
	mpa->sa_family = msa->in.sin_family;

	if (mpa->sa_family == AF_INET) {
		mpa->port = msa->in.sin_port;
		memcpy(&mpa->as.in_addr, &msa->in.sin_addr,
			sizeof(struct in_addr));
	} else {
		assert(mpa->sa_family == AF_INET6 && "invalid sa_family");
		mpa->port = msa->in6.sin6_port;
		mpa->as.in6_ptr = xmalloc(sizeof(struct in6_addr));
		memcpy(mpa->as.in6_ptr, &msa->in6.sin6_addr,
			sizeof(struct in6_addr));
	}
}
