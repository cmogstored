/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "cmogstored.h"
#include <arpa/inet.h>

/*
 * small replacement for getnameinfo(3), this only handles numeric types
 * for IPv4 and IPv6 and uses the compact mog_ni structure to reduce
 * stack usage in error reporting.
 */
void mog_nameinfo(struct mog_packaddr *mpa, struct mog_ni *ni)
{
	char *hostptr = ni->ni_host;
	size_t hostlen = sizeof(ni->ni_host) - (sizeof("[]") - 1);
	char *servptr = ni->ni_serv + 1; /* offset for ':' */
	size_t servlen = sizeof(ni->ni_serv) - 1; /* offset for ':' */
	int rc;
	const void *src;
	const char *ret;

	if (mpa->sa_family == AF_INET6) {
		hostptr[0] = '['; /* leading '[' */
		src = mpa->as.in6_ptr;
		hostptr++;
	} else {
		assert(mpa->sa_family == AF_INET && "bad family");
		src = &mpa->as.in_addr;
	}

	ret = inet_ntop(mpa->sa_family, src, hostptr, (socklen_t)hostlen);

	/* terminate serv string on error */
	assert(ret == hostptr && "inet_ntop");
	ni->ni_serv[0] = ':';

	/* add trailing ']' */
	if (mpa->sa_family == AF_INET6) {
		hostlen = strlen(hostptr);
		hostptr[hostlen] = ']';
		hostptr[hostlen + 1] = 0;
	}

	/* port number */
	rc = snprintf(servptr, servlen, "%u", (unsigned)ntohs(mpa->port));
	assert(rc > 0 && rc < (int)servlen && "we suck at snprintf");
}
