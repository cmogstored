/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "cmogstored.h"

/*
 * we block signals in pool threads, only the main thread receives signals
 */

static sigset_t fullset;
sigset_t mog_emptyset;

__attribute__((constructor)) static void sig_init(void)
{
	CHECK(int, 0, sigfillset(&fullset));
	CHECK(int, 0, sigemptyset(&mog_emptyset));
}

void mog_intr_disable(void)
{
	CHECK(int, 0, pthread_sigmask(SIG_SETMASK, &fullset, NULL));
}

void mog_intr_enable(void)
{
	CHECK(int, 0, pthread_sigmask(SIG_SETMASK, &mog_emptyset, NULL));
}

static int sleeper(struct timespec *tsp, const sigset_t *sigmask)
{
	int err = 0;

	/*
	 * pselect is a cancellation point,
	 * ppoll is not POSIX and is only a cancellation point on glibc.
	 */
	if (pselect(0, NULL, NULL, NULL, tsp, sigmask) < 0) {
		err = errno;
		assert((err == EINTR || err == ENOMEM) &&
		       "BUG in pselect usage");
	}
	return err;
}

/* thread-safe, interruptible sleep, negative seconds -> sleep forever */
int mog_sleep(long seconds)
{
	struct timespec ts;
	struct timespec *tsp;

	if (seconds < 0) {
		tsp = NULL;
	} else {
		ts.tv_sec = seconds;
		ts.tv_nsec = 0;
		tsp = &ts;
	}

	return sleeper(tsp, &mog_emptyset);
}
