/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "cmogstored.h"

%%{
	machine valid_put_path;
	main := "/dev"digit+ ('/'+) [^/] any+;
}%%

%% write data;

bool mog_valid_put_path(const char *buf, size_t len)
{
	const char *p, *pe;
	int cs;

	if (len <= 0)
		return false;
	if (buf[len - 1] == '/')
		return false;

	%% write init;

	p = buf;
	pe = buf + len;
	%% write exec;

	return cs != valid_put_path_error;
}
