/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */

/*
 * caller needs to setup: uint32_t *mog_devid = ...
 */
%%{
	machine path_parser;

	devid = "dev"
		(digit+) $ {
			/* no overflow checking here, we do it in mog_dev_new */
			*mog_devid *= 10;
			*mog_devid += fc - '0';
		}
		'/';
	# only stuff MogileFS will use
	mog_path_rest = [a-zA-Z0-9/\.\-]{0,36};
	mog_path = '/' (devid)? mog_path_rest;
}%%
