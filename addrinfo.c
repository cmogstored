/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "cmogstored.h"

/*
 * mog_listen_parse and mog_cfg_parse generate mog_addrinfo objects internally
 */
void mog_addrinfo_free(struct mog_addrinfo **aptr)
{
	struct mog_addrinfo *a = *aptr;

	if (!a) return;

	*aptr = NULL;
	mog_free(a->orig);
	freeaddrinfo(a->addr);
	free(a);
}
