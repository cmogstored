# this is only used for release-related tasks and should not
# be needed by normal development

include Rake::DSL if defined?(Rake::DSL)

url_base = "https://yhbt.net/cmogstored"
cgit_url = url_base + '.git'
git_url = 'https://yhbt.net/cmogstored.git'

def tags
  timefmt = '%Y-%m-%dT%H:%M:%SZ'
  @tags ||= `git tag -l`.split(/\n/).map do |tag|
    if %r{\Av[\d\.]+} =~ tag
      header, subject, body = `git cat-file tag #{tag}`.split(/\n\n/, 3)
      header = header.split(/\n/)
      tagger = header.grep(/\Atagger /).first
      body ||= "initial"
      {
        :time => Time.at(tagger.split(/ /)[-2].to_i).utc.strftime(timefmt),
        :tagger_name => %r{^tagger ([^<]+)}.match(tagger)[1].strip,
        :tagger_email => %r{<([^>]+)>}.match(tagger)[1].strip,
        :id => `git rev-parse refs/tags/#{tag}`.chomp!,
        :tag => tag,
        :subject => subject,
        :body => body,
      }
    end
  end.compact.sort { |a,b| b[:time] <=> a[:time] }
end

desc 'prints news as an Atom feed'
task :news_atom do
  require 'builder' # gem install builder
  new_tags = tags[0,10]
  x = Builder::XmlMarkup.new
  x.instruct! :xml, :encoding => 'UTF-8', :version => '1.0'
  x.feed(:xmlns => "http://www.w3.org/2005/Atom") do
    x.id "#{url_base}/NEWS.atom.xml"
    x.title "cmogstored news"
    x.subtitle "alternative mogstored implementation for MogileFS"
    x.link :rel => 'alternate', :type => 'text/plain',
           :href => "#{url_base}/NEWS"
    x.updated(new_tags.empty? ? "1970-01-01T00:00:00Z" : new_tags.first[:time])
    new_tags.each do |tag|
      x.entry do
        x.title tag[:subject]
        x.updated tag[:time]
        x.published tag[:time]
        x.author {
          x.name tag[:tagger_name]
          x.email tag[:tagger_email]
        }
        url = "#{cgit_url}/tag/?id=#{tag[:tag]}"
        x.link :rel => "alternate", :type => "text/html", :href => url
        x.id url
        x.content(:type =>:xhtml) do
          x.div(:xmlns => 'http://www.w3.org/1999/xhtml') do
            x.pre tag[:body]
          end
        end
      end
    end
  end
  puts x.target!
end

desc 'prints news as a text file'
task :news do
  title = "cmogstored news"
  puts title
  puts('-' * title.length)
  puts
  tags.each do |tag|
    time = tag[:time].tr!('T', ' ').gsub!(/:\d\dZ/, ' UTC')
    line = "#{tag[:tag].sub(/^v/, '')} / #{time}"
    puts line
    puts("-" * line.length)
    puts ""

    puts tag[:body].gsub(/^/m, "  ").gsub(/[ \t]+$/m, "")
    puts "" unless tag == tags.last
  end
end

desc "dump changelog to stdout"
task :changelog do
  since = "v1.0.0"

  puts "cmogstored changelog since #{since}"
  puts "Full changeset information is available at #{git_url}"
  puts "See NEWS file for a user-oriented summary of changes"
  puts ""

  cmd = %w(git log --pretty=medium --date=iso --decorate) << "#{since}.."
  system(*cmd) or abort $?
end

desc "compare dist tar.gz against contents in the git tree"
task :distcheck_git do
  tgz = ENV["TGZ"] or abort "TGZ= not specified"
  tgzfiles = `tar -ztf #{tgz}`.split(/\n/)
  tgzfiles.map! { |f| f.gsub!(%r{^[^/]+/}, '') }
  gitfiles = `git ls-files`.split(/\n/)
  gitonly = gitfiles - tgzfiles
  gitonly -= %w(build-aux/manpage-hack.mk)
  if gitonly[0]
    warn "The following files are missing from #{tgz}"
    warn ""
    gitonly.each { |f| warn f }
    exit(1)
  end
end
