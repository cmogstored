#!/usr/bin/awk -f
# This outputs 6 columns:
# PID FD BLOCKED_TIME RESCHEDULE_TIME METHOD PATH
#
# PID - pid of cmogstored process
# FD - descriptor of client
# BLOCKED_TIME - total time a client spent blocked
# RESCHEDULE_TIME - the time a client went from unblocked to dispatching
# METHOD - HTTP or sidechannel method (e.g. GET/HEAD/PUT/DELETE/MD5)
# PATH - path accessed (e.g. /dev666/usage)
#
# [PID,FD] - unique identifier on any host at that point-in-time
# BLOCKED_TIME - RESCHEDULE_TIME = time actually spent in the queue
# RESCHEDULE_TIME is usually very low.

/ ioq_blocked / {
	pid = $1
	fd = $2
	time = $4
	ioq_blocked[pid, fd] = time
}

/ ioq_reschedule / {
	pid = $1
	fd = $2
	time = $4
	ioq_reschedule[pid, fd] = time
}

/ http_req_start / {
	pid = $1
	fd = $2
	method = $4
	path = $5

	now = ioq_unblocked[pid, fd]
	if (now) {
		blocked = (now - ioq_blocked[pid, fd]) / 1000000
		resched = (now - ioq_reschedule[pid, fd]) / 1000000

		printf("% 6d % 6d %0.4f %0.4f %s %s\n",
			pid, fd, blocked, resched, method, path)
	}
}

/ mgmt_dig_start / {
	pid = $1
	fd = $2
	alg = $4
	path = $5

	now = ioq_unblocked[pid, fd]
	if (now) {
		blocked = (now - ioq_blocked[pid, fd]) / 1000000
		resched = (now - ioq_reschedule[pid, fd]) / 1000000

		printf("% 6d % 6d %0.4f %0.4f %s %s\n",
			pid, fd, blocked, resched, alg, path)
	}
}

/ ioq_unblocked / {
	pid = $1
	fd = $2
	now = $4
	ioq_unblocked[pid, fd] = now
}
