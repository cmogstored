#!/usr/bin/gawk -f
# using gawk for strftime
# This takes the output of the all.stp tapset distributed with cmogstored
# This emits output similar to Common Log Format (CLF), with two exceptions:
#
# 1) The byte count for PUT requests is recorded in byte bytes transferred
# since PUT responses are always an empty body with cmogstored.
# 2) elapsed time of the request is added.

/ http_accepted / {
	pid = $1
	fd = $2
	remote_addr = $4
	http_addr[pid,fd] = remote_addr
}

/ http_req_begin / {
	pid = $1
	fd = $2
	start_time = $4
	http_bytes_xfer[pid,fd] = "-"
	http_req_begin[pid,fd] = start_time
}

/ http_req_start / {
	pid = $1
	fd = $2
	method = $4
	path = $5
	http_req_start[pid,fd] = method " " path
}

/ http_res_start / {
	pid = $1
	fd = $2
	code = $4
	http_res_start[pid,fd] = code
}

/ http_bytes_xfer / {
	pid = $1
	fd = $2
	bytes = $4
	http_bytes_xfer[pid,fd] = bytes
}

/ http_res_done / {
	pid = $1
	fd = $2
	now_us = $4
	now_s = now_us / 1000000
	start_time = http_req_begin[pid,fd]
	bytes = http_bytes_xfer[pid,fd]
	remote_addr = http_addr[pid,fd]
	request = http_req_start[pid,fd]
	code = http_res_start[pid,fd]
	now = strftime("%d/%b/%Y:%H:%M:%S +0000", now_s)

	if (ELAPSED_TIME) {
		# the elapsed time is not part of CLF, but I personally find
		# log files worthless without it, so this is an option
		elapsed = sprintf(" %0.4f", (now_us - start_time)/1000000)
	} else {
		elapsed = ""
	}
	print(remote_addr " - - [" now "] \"" \
	      request " HTTP/1.1\" " code" "bytes elapsed)
}
