dnl systemtap support
dnl enable automatically if dtrace and sdt.h are available
AC_DEFUN([CM_SYSTEMTAP],[
AC_CHECK_PROGS(DTRACE, dtrace)
dnl require stap for now, too, as there are compatibility issues with
dnl dtrace under FreeBSD
AC_CHECK_PROGS(STAP, stap)
AC_CHECK_HEADER([sys/sdt.h], [SDT_H_FOUND='yes'], [SDT_H_FOUND='no'])
AS_IF([test "x$STAP" != x && test "x$DTRACE" != x && test $SDT_H_FOUND = yes],
	AC_CACHE_CHECK([for sys/sdt.h usability],
			[cm_cv_sdt_h_usable], [
			AC_TRY_COMPILE([
				#include <sys/sdt.h>
				void foo(void) { STAP_PROBE(foo, foo); }
			],[],
			[cm_cv_sdt_h_usable=yes],
			[cm_cv_sdt_h_usable=no])]))
AS_IF([test x"$cm_cv_sdt_h_usable" = xyes],
      [ENABLE_SYSTEMTAP=yes], [ENABLE_SYSTEMTAP=no])

dnl support explicit --disable-systemtap or --enable-systemtap
AC_MSG_CHECKING([whether to include systemtap tracing support])
AC_ARG_ENABLE([systemtap],
	[AS_HELP_STRING([--enable-systemtap],
	                [Enable inclusion of systemtap trace support])],
	[ENABLE_SYSTEMTAP="${enableval}"])
AM_CONDITIONAL([ENABLE_SYSTEMTAP], [test x$ENABLE_SYSTEMTAP = xyes])
AC_MSG_RESULT(${ENABLE_SYSTEMTAP})

dnl maybe somebody forced --enable-systemtap w/o dtrace or sdt.h
if test "x${ENABLE_SYSTEMTAP}" = xyes
then
	AS_IF([test "x${DTRACE}" = x], [AC_MSG_ERROR([dtrace not found])])
	AS_IF([test $SDT_H_FOUND = no],
	      [ AC_MSG_ERROR([systemtap support needs sys/sdt.h header]) ])
	AS_IF([test x"$cm_cv_sdt_h_usable" = xno],
	      [ AC_MSG_ERROR([sys/sdt.h header is not usable (clang?)]) ])
	AC_DEFINE([HAVE_SYSTEMTAP], [1], [Define to 1 if using sdt probes.])
fi
])
