dnl test for _all_ the GCC atomic builtins we use
dnl we need to test for CMPXCHG in particular since i386-only targets lack it
dnl even though other atomics may be available
AC_DEFUN([CM_GCC_ATOMICS],[
AC_REQUIRE([AC_CANONICAL_HOST])
AC_LANG_PUSH([C])
AS_CASE([$host_cpu], [[i[3456]86]], [
	AC_CACHE_CHECK([for GCC atomic builtins],
		[cm_cv_gcc_atomics], [
		AC_TRY_LINK([unsigned long a = 0;], [
			__sync_bool_compare_and_swap(&a, 0, 1);
			__sync_add_and_fetch(&a, 1);
			__sync_sub_and_fetch(&a, 1)
			],
			[cm_cv_gcc_atomics=yes],
			[cm_cv_gcc_atomics=no])])
	dnl try not to add -march=i486 unless we are certain it is needed
	AS_CASE([$cm_cv_gcc_atomics], [[no]], [
		unset cm_cv_gcc_atomics
		save_CFLAGS="$CFLAGS"
		CFLAGS="$CFLAGS -march=i486"
		AC_CACHE_CHECK([for GCC atomic builtins with -march=i486],
			[cm_cv_gcc_atomics], [
			 AC_TRY_LINK([unsigned long a = 0;], [
				__sync_bool_compare_and_swap(&a, 0, 1);
				__sync_add_and_fetch(&a, 1);
				__sync_sub_and_fetch(&a, 1)
				], [
				cm_cv_gcc_atomics=yes
				GCC_ATOMICS_CFLAGS=-march=i486
				],
				[cm_cv_gcc_atomics=no])])
		 CFLAGS="$save_CFLAGS"
	 ])
	 AS_CASE([$cm_cv_gcc_atomics],[[no]],
		[AC_MSG_ERROR([GCC atomic builtins not available])])
AC_SUBST(GCC_ATOMICS_CFLAGS)
])
AC_LANG_POP
]) dnl CM_GCC_ATOMICS
