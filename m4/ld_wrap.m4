dnl check for --wrap support in ld (expected of GNU ld)
AC_DEFUN([CM_LD_WRAP],[
AC_CACHE_CHECK([if gcc/ld supports -Wl,--wrap],
[cm_cv_ld_wrap],
[if test "$enable_shared" = no
then
	cm_cv_ld_wrap="not needed, shared libraries are disabled"
else
	cm_ldflags_save="$LDFLAGS"
	LDFLAGS="-Wl,--wrap=free"
	AC_LINK_IFELSE([AC_LANG_PROGRAM([
		#include <stdlib.h>
		int __real_free(void *ptr);
		static void __wrap_free(void *ptr) { __real_free(ptr); }
		],[ free(NULL); ]
	)],
	[cm_cv_ld_wrap=yes],
	[cm_cv_ld_wrap=no])
	LDFLAGS="$cm_ldflags_save"
fi])
	AM_CONDITIONAL([HAVE_LD_WRAP], test "x$cm_cv_ld_wrap" = "xyes")
])
