/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
%%{
	machine listen_parser_common;

	ipv4 = (digit+ '.' digit+ '.' digit+ '.' digit+)
		> { mark_beg = fpc; }
		@ {
			mark_len = fpc - mark_beg + 1;
			sa_family = AF_INET;
		};
	ipv6 = '['
			((xdigit|':')+)
			> { mark_beg = fpc; }
			@ { mark_len = fpc - mark_beg + 1; }
		']' @ { sa_family = AF_INET6; };
	port = (digit+)
		> { port_beg = fpc; }
		@ { port_len = fpc - port_beg + 1; };

	listen = (((ipv4|ipv6)? ':')? port ) $! {
		syslog(LOG_ERR, "bad character in IP address: %c", fc);
	};
}%%
