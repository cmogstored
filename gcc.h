/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#define MOG_PRINTF __attribute__((format(printf,1,2)))
#define MOG_LIKELY(x) (__builtin_expect((x), 1))
#define MOG_UNLIKELY(x) (__builtin_expect((x), 0))
#define MOG_NOINLINE __attribute__((noinline))
#define MOG_CHECK __attribute__((warn_unused_result))
#define mog_sync_add_and_fetch(dst,val) __sync_add_and_fetch((dst),(val))
#define mog_sync_sub_and_fetch(dst,val) __sync_sub_and_fetch((dst),(val))

/* need the synchronization, right? */
#define mog_sync_fetch(dst) mog_sync_add_and_fetch((dst),0)
