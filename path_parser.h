/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "config.h"
#include <stdint.h>

static void mog_devid_incr(uint32_t *mog_devid, unsigned c)
{
	*mog_devid *= 10;
	*mog_devid += c - '0';

	/* no overflow checking here */
}
