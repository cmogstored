/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "check.h"
static struct mog_svc svc = { .persist_client = 1 };
static struct mog_http xhttp;
static struct mog_http *http = &xhttp;
static char *buf;
static size_t len;
static enum mog_parser_state state;

static void assert_path_equal(const char *str)
{
	size_t slen = strlen(str);

	assert(0 == memcmp(str, buf + http->_p.path_tip, slen));
	assert(http->_p.path_end == http->_p.path_tip + slen);
}

static void reset(void)
{
	free(buf);
	mog_http_init(http, &svc);
}

static void buf_set(const char *s)
{
	reset();
	buf = xstrdup(s);
	len = strlen(s);
}

int main(void)
{
	if ("normal HTTP GET request") {
		buf_set("GET /foo HTTP/1.1\r\nHost: 127.6.6.6\r\n\r\n");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.http_method == MOG_HTTP_METHOD_GET
		       && "http_method should be GET");
		assert(http->_p.persistent && "not persistent");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert_path_equal("/foo");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("normal HTTP GET request with redundant leading slash") {
		buf_set("GET //foo HTTP/1.1\r\nHost: 127.6.6.6\r\n\r\n");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.http_method == MOG_HTTP_METHOD_GET
		       && "http_method should be GET");
		assert(http->_p.persistent && "not persistent");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert_path_equal("/foo");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("HTTP/1.1 request with explicit close") {
		buf_set("GET /foo HTTP/1.1\r\n"
		        "Host: 127.6.6.6\r\n"
		        "Connection: close\r\n"
		        "\r\n");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.http_method == MOG_HTTP_METHOD_GET
		       && "http_method should be GET");
		assert(http->_p.persistent == 0 && "should not be persistent");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert_path_equal("/foo");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("HTTP/1.0 request with keepalive") {
		buf_set("GET /foo HTTP/1.0\r\n"
		        "Connection:\r\n keep-alive\r\n"
		        "\r\n");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.http_method == MOG_HTTP_METHOD_GET
		       && "http_method should be GET");
		assert(http->_p.persistent == 1 && "should be persistent");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert_path_equal("/foo");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("bogus HTTP/1.0 request") {
		buf_set("GET /foo HTTP/1.0\r\n"
		        "Connection:\r\nkeep-alive\r\n"
		        "\r\n");
		state = mog_http_parse(http, buf, len);
		assert(state == MOG_PARSER_ERROR && "parser not errored");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("bogus request") {
		buf_set("adlkf;asdfj\r\n'GET /foo HTTP/1.0\r\n"
		        "Connection:\r\nkeep-alive\r\n"
		        "\r\n");
		state = mog_http_parse(http, buf, len);
		assert(state == MOG_PARSER_ERROR && "parser not errored");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("HTTP/1.1 HEAD request") {
		buf_set("HEAD /foo HTTP/1.1\r\n"
		        "Host: 127.6.6.6\r\n"
		        "\r\n");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.http_method == MOG_HTTP_METHOD_HEAD
		       && "http_method should be HEAD ");
		assert(http->_p.persistent == 1 && "should be persistent");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert_path_equal("/foo");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("HTTP/1.1 PUT request") {
		buf_set("PUT /foo HTTP/1.1\r\n"
		        "Host: 127.6.6.6\r\n"
		        "Content-Length: 12345\r\n"
		        "\r\n"
		        "partial body request");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.content_len == 12345);
		assert(http->_p.http_method == MOG_HTTP_METHOD_PUT
		       && "http_method should be PUT");
		assert(http->_p.persistent == 1 && "should be persistent");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert_path_equal("/foo");
		assert(strcmp(buf + http->_p.buf_off, "partial body request")
		       == 0 && "buffer repositioned to body start");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("HTTP/1.1 PUT chunked request header") {
		buf_set("PUT /foo HTTP/1.1\r\n"
		        "Host: 127.6.6.6\r\n"
		        "Transfer-Encoding: chunked\r\n"
		        "\r\n"
		        "16\r\npartial...");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.chunked);
		assert(http->_p.has_md5 == 0);
		assert(http->_p.http_method == MOG_HTTP_METHOD_PUT
		       && "http_method should be PUT");
		assert(http->_p.persistent == 1 && "should be persistent");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert_path_equal("/foo");
		assert(strcmp(buf + http->_p.buf_off, "16\r\npartial...") == 0
		       && "buffer repositioned to body start");
		assert(!http->_p.usage_txt && "not a usage request");
	}
	if ("HTTP/1.1 PUT Transfer-Encoding: bogus header") {
		buf_set("PUT /foo HTTP/1.1\r\n"
		        "Host: 127.6.6.6\r\n"
		        "Transfer-Encoding: bogus\r\n"
		        "\r\n"
		        "16\r\npartial...");
		state = mog_http_parse(http, buf, len);
		assert(state == MOG_PARSER_ERROR && "parser not errored");
	}

	if ("HTTP/1.1 PUT with Content-Range") {
		buf_set("PUT /foo HTTP/1.1\r\n"
		        "Host: 127.6.6.6\r\n"
		        "Transfer-Encoding: chunked\r\n"
		        "Content-Range: bytes 666-666666/*\r\n"
		        "\r\n"
		        "16\r\npartial...");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.range_beg == 666);
		assert(http->_p.range_end == 666666);
		assert(http->_p.has_content_range == 1);
		assert(http->_p.has_md5 == 0);
		assert(http->_p.http_method == MOG_HTTP_METHOD_PUT
		       && "http_method should be PUT");
		assert(http->_p.persistent == 1 && "should be persistent");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert_path_equal("/foo");
		assert(strcmp(buf + http->_p.buf_off, "16\r\npartial...") == 0
		       && "buffer repositioned to body start");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("HTTP/1.1 PUT chunked request header w/Trailer") {
		buf_set("PUT /foo HTTP/1.1\r\n"
		        "Host: 127.6.6.6\r\n"
		        "Transfer-Encoding: chunked\r\n"
			"Trailer: Content-MD5\r\n"
		        "\r\n"
		        "16\r\npartial...");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.chunked);
		assert(http->_p.has_md5 == 1);
		assert(http->_p.http_method == MOG_HTTP_METHOD_PUT
		       && "http_method should be PUT");
		assert(http->_p.persistent == 1 && "should be persistent");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert_path_equal("/foo");
		assert(strcmp(buf + http->_p.buf_off, "16\r\npartial...") == 0
		       && "buffer repositioned to body start");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("HTTP/1.1 DELETE request") {
		buf_set("DELETE /foo HTTP/1.1\r\n"
		        "Host: 127.6.6.6\r\n"
		        "\r\n");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.content_len == 0);
		assert(http->_p.has_md5 == 0);
		assert(http->_p.http_method == MOG_HTTP_METHOD_DELETE
		       && "http_method should be DELETE");
		assert(http->_p.persistent == 1 && "should be persistent");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert_path_equal("/foo");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("HTTP/1.1 MKCOL request") {
		buf_set("MKCOL /foo HTTP/1.1\r\n"
		        "Host: 127.6.6.6\r\n"
		        "\r\n");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.content_len == 0);
		assert(http->_p.has_md5 == 0);
		assert(http->_p.http_method == MOG_HTTP_METHOD_MKCOL
		       && "http_method should be MKCOL");
		assert(http->_p.persistent == 1 && "should be persistent");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert_path_equal("/foo");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("HTTP/1.1 Range (mid) GET request") {
		buf_set("GET /foo HTTP/1.1\r\n"
		        "Host: 127.6.6.6\r\n"
		        "Range: bytes=5-55\r\n"
		        "\r\n");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.has_range == 1);
		assert(http->_p.range_beg == 5 && "range_beg didn't match");
		assert(http->_p.range_end == 55 && "range_end didn't match");
		assert(http->_p.http_method == MOG_HTTP_METHOD_GET
		       && "http_method should be GET");
		assert(http->_p.persistent == 1 && "should be persistent");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert_path_equal("/foo");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("HTTP/1.1 Range (tip) GET request") {
		buf_set("GET /foo HTTP/1.1\r\n"
		        "Host: 127.6.6.6\r\n"
		        "Range: bytes=-55\r\n"
		        "\r\n");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.has_range == 1);
		assert(http->_p.range_beg == -1 && "range_beg didn't match");
		assert(http->_p.range_end == 55 && "range_end didn't match");
		assert(http->_p.http_method == MOG_HTTP_METHOD_GET
		       && "http_method should be GET");
		assert(http->_p.persistent == 1 && "should be persistent");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert_path_equal("/foo");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("HTTP/1.1 Range (end) GET request") {
		buf_set("GET /foo HTTP/1.1\r\n"
		        "Host: 127.6.6.6\r\n"
		        "Range: bytes=55-\r\n"
		        "\r\n");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.has_range == 1);
		assert(http->_p.range_beg == 55 && "range_beg didn't match");
		assert(http->_p.range_end == -1 && "range_end didn't match");
		assert(http->_p.http_method == MOG_HTTP_METHOD_GET
		       && "http_method should be GET");
		assert(http->_p.persistent == 1 && "should be persistent");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert_path_equal("/foo");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("HTTP/1.1 devid parse") {
		buf_set("GET /dev666/0/1.fid HTTP/1.0\r\n"
		        "\r\n");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.http_method == MOG_HTTP_METHOD_GET
		       && "http_method should be GET");
		assert(http->_p.mog_devid == 666 && "dev666 set");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert_path_equal("/dev666/0/1.fid");
		assert(!http->_p.usage_txt && "not a usage request");
	}

	if ("HTTP/1.0 devN/usage request") {
		buf_set("GET /dev666/usage HTTP/1.0\r\n\r\n");
		state = mog_http_parse(http, buf, len);
		assert(http->_p.http_method == MOG_HTTP_METHOD_GET
		       && "http_method should be GET");
		assert(http->_p.mog_devid == 666 && "dev666 set");
		assert(state == MOG_PARSER_DONE && "parser not done");
		assert(http->_p.usage_txt && "a usage request");

		buf_set("GET /dev666/usager HTTP/1.0\r\n\r\n");
		state = mog_http_parse(http, buf, len);
		assert(!http->_p.usage_txt && "a usage request");

		buf_set("GET /dev666/usag HTTP/1.0\r\n\r\n");
		state = mog_http_parse(http, buf, len);
		assert(!http->_p.usage_txt && "a usage request");
	}

	reset();
	return 0;
}
