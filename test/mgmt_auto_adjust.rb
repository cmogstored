#!/usr/bin/env ruby
# -*- encoding: binary -*-
# Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
# License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
require 'test/test_helper'

class TestMgmtAutoAdjust < Test::Unit::TestCase
  def setup
    @tmpdir = Dir.mktmpdir('cmogstored-mgmt-test')
    @to_close = []
    @host = TEST_HOST
    srv = TCPServer.new(@host, 0)
    @port = srv.addr[1]
    srv.close

    http = TCPServer.new(@host, 0)
    @httpport = http.addr[1]
    http.close

    @err = Tempfile.new("stderr")
    Dir.mkdir("#@tmpdir/dev666")
    cmd = [ "cmogstored", "--docroot=#@tmpdir", "--mgmtlisten=#@host:#@port",
            "--httplisten=#@host:#@httpport",
            "--maxconns=500" ]
    vg = ENV["VALGRIND"] and cmd = vg.split(/\s+/).concat(cmd)
    @pid = fork {
      ENV["MOG_DISK_USAGE_INTERVAL"] = "1"
      $stderr.reopen(@err)
      @err.close
      exec(*cmd)
    }
    @client = get_client
  end

  def teardown
    Process.kill(:QUIT, @pid) rescue nil
    _, status = Process.waitpid2(@pid)
    @to_close.each { |io| io.close unless io.closed? }
    FileUtils.rm_rf(@tmpdir)
    @err.rewind
    # $stderr.write(@err.read)
    assert status.success?, status.inspect
  end

  def wait_for_seen_dev
    @client.write "watch\r\n"
    seen_dev = false
    while line = @client.gets
      line.strip!
      case line
      when /^666\t/
        seen_dev = true
      when "."
        break if seen_dev
      end
    end
  end

  def test_aio_threads_auto_adjust
    wait_for_seen_dev
    t_yield # wait for threads to spawn
    taskdir = "/proc/#@pid/task"
    glob = "#{taskdir}/*"
    prev_threads = Dir[glob].size if File.directory?(taskdir)
    if RUBY_PLATFORM =~ /linux/
      assert File.directory?(taskdir), "/proc not mounted on Linux?"
    end

    File.directory?(taskdir) or return
    Dir.mkdir("#@tmpdir/dev333")
    stop = Time.now + 10
    while prev_threads == Dir[glob].size && Time.now < stop
      sleep(0.1)
    end
    cur_threads = Dir[glob].size
    assert_operator cur_threads, :>, prev_threads,
           "prev_threads=#{cur_threads} > prev_threads=#{prev_threads}"
    assert_match(%r{updating server aio_threads=20}, File.read(@err.path))
  end if `which iostat 2>/dev/null`.chomp.size != 0 &&
         RUBY_PLATFORM !~ /kfreebsd-gnu/

  def test_aio_threads_auto_adjust_warn
    at = Time.now
    @client.write "server aio_threads = 1\r\n"
    assert_equal "\r\n", @client.gets
    stop = Time.now + 60
    expect = "server aio_threads=1"
    while (! File.read(@err.path).include?(expect) && Time.now < stop)
      sleep 0.1
    end
    msg = File.read(@err.path)
    assert(msg.include?(expect), "msg[#@pid]: #{msg} #{at}")

    Dir.mkdir("#@tmpdir/dev333")
    warning = "fewer aio_threads(1) than devices(2)"
    stop = Time.now + 10
    while (! File.read(@err.path).include?(warning)) && Time.now < stop
      sleep 0.1
    end
    msg = File.read(@err.path)
    assert(msg.include?(warning), "msg[#@pid]: #{msg} #{at}")
  end
end
