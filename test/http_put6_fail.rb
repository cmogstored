#!/usr/bin/env ruby
# -*- encoding: binary -*-
# Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
# License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
require 'test/test_helper'
require 'net/http'
require 'stringio'

class TestHTTPPut6Fail < Test::Unit::TestCase
  def setup
    @skip = true
    @tmpdir = Dir.mktmpdir('cmogstored-httpput6-test')
    Dir.mkdir("#@tmpdir/dev666")
    @to_close = []
    @host = TEST_HOST6
    srv = TCPServer.new(@host, 0)
    @port = srv.addr[1]
    srv.close
    @err = Tempfile.new("stderr")
    cmd = [ "cmogstored", "--docroot=#@tmpdir", "--httplisten=[#@host]:#@port",
            "--maxconns=500" ]
    vg = ENV["VALGRIND"] and cmd = vg.split(/\s+/).concat(cmd)
    @pid = fork {
      $stderr.reopen(@err)
      @err.close
      exec(*cmd)
    }
    @client = get_client
    @skip = false
  rescue Errno::EAFNOSUPPORT
    # host does not have IPv6
  end

  def teardown
    return if @skip
    Process.kill(:QUIT, @pid) rescue nil
    _, status = Process.waitpid2(@pid)
    @to_close.each { |io| io.close unless io.closed? }
    @err.rewind
    assert status.success?, status.inspect
  ensure
    FileUtils.rm_rf(@tmpdir)
  end

  def test_put_premature_eof
    return if @skip
    path = "/dev666/foo"
    url = "http://[#@host]:#@port#{path}"
    req = "PUT #{url} HTTP/1.1\r\n" \
          "Host: [#@host]:#@port\r\n" \
          "Content-Length: 666\r\n" \
          "\r\n" \
          "abcde"
    @client.write(req)
    @client.shutdown(Socket::SHUT_WR)
    addr = Regexp.escape("[#{@client.addr[3]}]:#{@client.addr[1]}")
    assert_nil @client.read(1)
    assert ! File.exist?("#@tmpdir#{path}")
    buf = File.read(@err.path)
    assert_match(%r{PUT #{path} failed from #{addr} after 5 bytes:}, buf)
    if RUBY_PLATFORM =~ /linux/
      assert_match(%r{last_data_recv=\d+ms from #{addr} for PUT #{path}}, buf)
    end
  end

  def test_put_premature_eof_chunked
    return if @skip
    path = "/dev666/foo"
    url = "http://[#@host]:#@port#{path}"
    req = "PUT #{url} HTTP/1.1\r\n" \
          "Host: [#@host]:#@port\r\n" \
          "Transfer-Encoding: chunked\r\n" \
          "\r\n" \
          "666\r\nf"
    @client.write(req)
    @client.shutdown(Socket::SHUT_WR)
    addr = Regexp.escape("[#{@client.addr[3]}]:#{@client.addr[1]}")
    assert_nil @client.read(1)
    assert ! File.exist?("#@tmpdir#{path}")
    buf = File.read(@err.path)
    assert_match(%r{PUT #{path} failed from #{addr} after 1 bytes:}, buf)
    if RUBY_PLATFORM =~ /linux/
      assert_match(%r{last_data_recv=\d+ms from #{addr} for PUT #{path}}, buf)
    end
  end
end
