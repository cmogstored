#!/usr/bin/env ruby
# -*- encoding: binary -*-
# Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
# License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
require 'test/test_helper'
require 'timeout'

class TestMgmtIostat < Test::Unit::TestCase
  TEST_PATH = File.dirname(__FILE__) + ":#{ENV['PATH']}"
  RUBY = ENV["RUBY"] || "ruby"

  def iostat_mock
    "#{RUBY} #{File.dirname(__FILE__)}/iostat-mock.rb"
  end

  def setup
    @iostat_pid = Tempfile.new('testt-iostat-pid')
    @tmpdir = Dir.mktmpdir('cmogstored-mgmt-iostat-test')
    @host = TEST_HOST
    srv = TCPServer.new(@host, 0)
    @port = srv.addr[1]
    srv.close
    cmd = [ "cmogstored", "--docroot=#@tmpdir", "--mgmtlisten=#@host:#@port",
            "--maxconns=500" ]
    vg = ENV["VALGRIND"] and cmd = vg.split(/\s+/).concat(cmd)
    @cmd = cmd
    @pid = nil
    @to_close = []
  end

  def teardown
    @to_close.each { |io| io.close unless io.closed? }
    if @pid
      Process.kill(:QUIT, @pid) rescue nil
      _, status = Process.waitpid2(@pid)
      assert status.success?, status.inspect
    end
    FileUtils.rm_rf(@tmpdir)
  end

  def __test_iostat_dies(workers = false)
    Dir.mkdir "#@tmpdir/dev666"
    err = Tempfile.new('err')
    @pid = fork do
      ENV["PATH"] = TEST_PATH
      ENV["MOG_IOSTAT_CMD"] = "#{iostat_mock} #{@iostat_pid.path} slow"
      $stderr.reopen(err.path, 'a')
      @cmd << workers if workers
      exec(*@cmd)
    end
    iostat_pid = nil
    Timeout.timeout(30) do
      begin
        iostat_pid = @iostat_pid.read.to_i
      end while iostat_pid == 0 && sleep(0.05)
      Process.kill(:TERM, iostat_pid)
    end

    expect = %r{iostat done \(pid=#{iostat_pid}, status=\d}
    Timeout.timeout(30) do
      loop do
        stderr = File.read(err.path)
        break if stderr =~ expect
        sleep 0.05
      end
    end
  ensure
    err.close!
  end

  def test_iostat_dies
    __test_iostat_dies
  end

  def test_iostat_dies_with_workers
    __test_iostat_dies("--worker-processes=1")
  end

  def test_iostat_fast
    Dir.mkdir "#@tmpdir/dev666"
    @pid = fork do
      ENV["PATH"] = TEST_PATH
      ENV["MOG_IOSTAT_CMD"] = "#{iostat_mock} #{@iostat_pid.path} fast"
      exec(*@cmd)
    end

    og = get_client
    og.write "watch\n"
    threads = []
    nr = RUBY_PLATFORM =~ /linux/ ? 400 : 10
    nr.times do
      threads << Thread.new do
        c = get_client
        assert_equal 6, c.write("watch\n")
        100.times { assert_kind_of(String, c.gets) }
        c
      end
    end

    sleep 1
    threads.each { |th| th.value.close }
    assert og.readpartial(16384)
    sleep 1
    assert og.read_nonblock(512)
    sleep 1
    assert og.read_nonblock(512)
    iostat_pid = @iostat_pid.read.to_i
    if iostat_pid > 0
      Process.kill(:TERM, iostat_pid)
    end
  end

  def test_iostat_bursty1
    iostat_edge_case("bursty1")
  end

  def test_iostat_bursty2
    iostat_edge_case("bursty2")
  end

  def test_iostat_slow
    iostat_edge_case("slow")
  end

  def iostat_edge_case(type)
    Dir.mkdir "#@tmpdir/dev666"
    @pid = fork do
      ENV["PATH"] = TEST_PATH
      ENV["MOG_IOSTAT_CMD"] = "#{iostat_mock} #{@iostat_pid.path} #{type}"
      exec(*@cmd)
    end

    og = get_client
    og.write "watch\n"
    5.times do
      assert_match(/\n$/, x = og.gets)
      p(x) if $DEBUG
    end
    og.close
  end
end
