/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "check.h"

int main(void)
{
	struct mog_fd *mfd;
	int open_max = (int)sysconf(_SC_OPEN_MAX);
	int i;

	mfd = mog_fd_init(0, MOG_FD_TYPE_UNUSED);
	{
		struct mog_mgmt *mgmt = &mfd->as.mgmt;

		assert(mog_fd_of(mgmt) == mfd);
	}

	for (i = 0; i < open_max; i++) {
		mfd = mog_fd_init(i, MOG_FD_TYPE_UNUSED);
		assert(mfd && "mfd unset");
	}

	return 0;
}
