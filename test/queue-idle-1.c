/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "check.h"
static int fds[2];
static char buf[128];
static struct mog_queue *q;
static struct mog_fd *mfd;

static void setup(void)
{
	q = mog_queue_new();
	pipe_or_die(fds);
	mfd = mog_fd_init(fds[0], MOG_FD_TYPE_UNUSED);

	mog_set_nonblocking(fds[0], true);
	assert(read(fds[0], buf, sizeof(buf)) == -1 &&
	       errno == EAGAIN && "read() should EAGAIN");
}

static void teardown(void)
{
	close_pipe(fds);
}

static void test_nonblocking(void)
{
	setup();

	mog_idleq_add(q, mfd, MOG_QEV_RD);
	assert(NULL == mog_idleq_wait_intr(q, 0)
	       && "q wait should return NULL");
	assert(1 == write(fds[1], ".", 1) && "couldn't write");
	assert(mfd == mog_idleq_wait_intr(q, 0) && "q wait should return mfd");

	teardown();
}

static void * wait_then_write(void *arg)
{
	sleep(1);
	assert(1 == write(fds[1], "B", 1) && "couldn't write");

	return NULL;
}

static void test_blocking(void)
{
	pthread_t thr;

	setup();

	mog_idleq_add(q, mfd, MOG_QEV_RD);
	CHECK(int, 0, pthread_create(&thr, NULL, wait_then_write, NULL));
	printf("start wait: %d\n", (int)time(NULL));
	assert(mfd == mog_idleq_wait_intr(q, -1));
	printf("  end wait: %d\n", (int)time(NULL));
	assert(1 == read(fds[0], buf, 1) && "read failed");
	assert(buf[0] == 'B' && "didn't read expected 'B'");
	assert(0 == pthread_join(thr, NULL) && "pthread_join failed");

	teardown();
}

int main(void)
{
	test_nonblocking();
	test_blocking();

	return 0;
}
