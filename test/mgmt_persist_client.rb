#!/usr/bin/env ruby
# -*- encoding: binary -*-
require 'test/test_helper'
require 'net/http'

class TestMgmtPersistClient < Test::Unit::TestCase
  def setup
    @tmpdir = Dir.mktmpdir('mgmt-persist-client-test')
    @host = TEST_HOST

    srv = TCPServer.new(@host, 0)
    @port = srv.addr[1]
    srv.close

    srv = TCPServer.new(@host, 0)
    @httpport = srv.addr[1]
    srv.close

    @pid = nil
    @to_close = []
    @err = Tempfile.new("stderr")
    Dir.mkdir("#@tmpdir/dev666")
    cmd = [ "cmogstored", "--docroot=#@tmpdir", "--mgmtlisten=#@host:#@port",
            "--httplisten=#@host:#@httpport",
            "--maxconns=500" ]
    vg = ENV["VALGRIND"] and cmd = vg.split(/\s+/).concat(cmd)
    @pid = fork {
      $stderr.reopen(@err)
      @err.close
      exec(*cmd)
    }
    @client = get_client
  end

  def test_persist_toggle
    {
      "off" => "on",
      "false" => "true",
      "0" => "1",
      "no" => "yes",
    }.each do |f,t|
      @client.write "SET mogstored.persist_client = #{f}\r\n"
      assert_equal "\r\n", @client.gets
      Net::HTTP.start(@host, @httpport) do |http|
        resp = http.request(Net::HTTP::Head.new('/'))
        assert_kind_of Net::HTTPOK, resp
        assert_equal "close", resp["Connection"]
      end

      @client.write "SET mogstored.persist_client = #{t}\r\n"
      assert_equal "\r\n", @client.gets
      Net::HTTP.start(@host, @httpport) do |http|
        resp = http.request(Net::HTTP::Head.new('/'))
        assert_kind_of Net::HTTPOK, resp
        assert_equal "keep-alive", resp["Connection"]
      end
    end
  end

  def teardown
    @to_close.each { |io| io.close unless io.closed? }
    FileUtils.rm_rf(@tmpdir)
    if @pid
      Process.kill(:QUIT, @pid) rescue nil
      _, status = Process.waitpid2(@pid)
      assert status.success?, status.inspect
    end
  end
end
