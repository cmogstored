/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#ifdef HAVE_SYSTEMTAP
#  include "probes.h"
#  define TRACE(probe) probe
#  define TRACE_ENABLED(probe) probe ## _ENABLED()
#else  /*  HAVE_SYSTEMTAP */
#  define TRACE(probe)
#  define TRACE_ENABLED(probe) (0)
#endif /* !HAVE_SYSTEMTAP */
