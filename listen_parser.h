/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
struct mog_addrinfo *
mog_listen_parse_internal(char *, size_t, char *, size_t, sa_family_t);
