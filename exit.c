/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "cmogstored.h"
#include "nostd/setproctitle.h"

static void acceptor_quit(struct mog_fd **mfdp)
{
	struct mog_fd *mfd = *mfdp;

	if (mfd) {
		struct mog_accept *ac = &mfd->as.accept;

		mog_thrpool_quit(&ac->thrpool, NULL);
		mog_fd_put(mfd);
		*mfdp = NULL;
	}
}

static bool svc_quit_i(void *svcptr, void *ignored)
{
	struct mog_svc *svc = svcptr;

	svc->persist_client = 0;
	mog_svc_dev_quit_prepare(svc);
	acceptor_quit(&svc->mgmt_mfd);
	acceptor_quit(&svc->http_mfd);
	acceptor_quit(&svc->httpget_mfd);

	return true;
}

static bool svc_requeue_prepare(void *svcptr, void *queue)
{
	struct mog_svc *svc = svcptr;

	svc->queue = queue;

	mog_svc_dev_requeue_prepare(svc);

	return true;
}

_Noreturn void cmogstored_exit(void)
{
	/* call atexit() handlers and make valgrind happy */
	setproctitle("cmogstored, shutting down");
	mog_svc_each(svc_quit_i, NULL);
	mog_svc_dev_shutdown();
	mog_queue_stop(mog_notify_queue);
	mog_svc_dev_shutdown();
	mog_svc_each(svc_requeue_prepare, mog_notify_queue);
	mog_fdmap_requeue(mog_notify_queue);
	mog_queue_quit_loop(mog_notify_queue);
	exit(EXIT_SUCCESS);
}
