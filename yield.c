/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "cmogstored.h"
#ifndef HAVE_PTHREAD_YIELD
#  define pthread_yield() (void)sched_yield()
#endif

/*
 * pthread_yield may migrate us to the same CPU as the task we're waiting
 * on, so just keep yielding for every CPU we have as this throttles
 * our ability to spam SIGURG.  This means the threads we're trying to
 * gracefully kill off can finish their work and check their mog_do_quit
 * flag sooner
 *
 * We only use this as a last resort when normal wakeups/notifications
 * are not usable (e.g. recovering from out-of-resource problems)
 */
void mog_yield(void)
{
	static unsigned long nproc_all;
	unsigned long i;

	if (!nproc_all)
		nproc_all = num_processors(NPROC_ALL) * 2;
	for (i = 0; i < nproc_all; i++)
		pthread_yield();
}
