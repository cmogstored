/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
struct mog_queue;
struct mog_fd;
#define MOG_IOUTIL_LEN (sizeof("1666.00"))

/* this is way larger than it needs to be... */
#define MOG_IOSTAT_DEVLEN (72)

struct mog_iostat {
	int cs;
	bool ready;
	uint8_t util_tip;
	uint8_t dev_tip;
	struct mog_queue *queue;
	char util[MOG_IOUTIL_LEN];
	char dev[MOG_IOSTAT_DEVLEN];
};

void mog_iostat_init(struct mog_iostat *);
enum mog_parser_state
mog_iostat_parse(struct mog_iostat *, char *buf, size_t len);
void mog_iostat_commit(void);
void mog_iostat_line_done(struct mog_iostat *);
void mog_iostat_queue_step(struct mog_fd *);
