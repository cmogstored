/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#define MOG_DEFAULT_MAXCONNS 10000
#define MOG_DEFAULT_HTTPLISTEN "0.0.0.0:7500"
#define MOG_DEFAULT_MGMTLISTEN "0.0.0.0:7501"
#define MOG_DEFAULT_DOCROOT "/var/mogdata"
#define MOG_DEFAULT_CONFIGFILE "/etc/mogilefs/mogstored.conf"
#define MOG_DEVID_MAX (16777215) /* MEDIUMINT in DB */

/* TODO: update if MogileFS supports FIDs >= 10,000,000,000 */
#define MOG_PATH_MAX (sizeof("/dev16777215/0/000/000/0123456789.fid"))
