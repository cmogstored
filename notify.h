/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
enum mog_notification {
	MOG_NOTIFY_SIGNAL = -1,
	MOG_NOTIFY_DEVICE_REFRESH = 0,
	MOG_NOTIFY_AIO_THREADS = 1,
	MOG_NOTIFY_MAX
};

extern struct mog_queue *mog_notify_queue;
