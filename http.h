/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "iov_str.h"
void mog_http_resp0(struct mog_fd *, struct iovec *status, bool alive);

#define mog_http_resp(mfd,conststr,alive) do { \
	struct iovec statustmp; \
	IOV_STR(&statustmp, (conststr)); \
	mog_http_resp0((mfd), &statustmp, (alive)); \
} while (0)
