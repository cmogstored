/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "cmogstored.h"
static Hash_table *listeners; /* yes, we'll scale to 10K listen sockets, L10K! */

struct listener {
	union mog_sockaddr msa;
	socklen_t len;
	int fd;
};

static bool listener_cmp(const void *a, const void *b)
{
	const struct listener *la = a;
	const struct listener *lb = b;

	return (la->len == lb->len) &&
	       (memcmp(&la->msa, &lb->msa, lb->len) == 0);
}

static size_t listener_hash(const void *x, size_t tablesize)
{
	const struct listener *l = x;
	size_t value = 0;
	socklen_t i;

	for (i = 0; i < l->len; i++)
		value = (value * 31 + l->msa.bytes[i]) % tablesize;

	return value;
}

static void register_listen_fd(int fd)
{
	struct listener tmp;
	struct listener *ins;
	struct mog_ni ni;
	struct mog_packaddr mpa;
	struct sockaddr *sa = &tmp.msa.sa;

	tmp.len = (socklen_t)sizeof(tmp.msa);
	if (getsockname(fd, sa, &tmp.len) != 0)
		die_errno("getsockname(fd=%d) failed", fd);

	if (sa->sa_family != AF_INET && sa->sa_family != AF_INET6)
		die("invalid address family=%d (not AF_INET/AF_INET6)",
		    (int)sa->sa_family);

	mog_packaddr_init(&mpa, &tmp.msa, tmp.len);

	mog_nameinfo(&mpa, &ni);

	syslog(LOG_INFO, "inherited %s%s on fd=%d", ni.ni_host, ni.ni_serv, fd);

	ins = xmalloc(sizeof(*ins));
	*ins = tmp;
	ins->fd = fd;

	switch (hash_insert_if_absent(listeners, ins, NULL)) {
	case 0:
		die("duplicate listener %s%s on fd=%d",
			ni.ni_host, ni.ni_serv, fd);
		break;
	case 1: /* success */
		return;
	default:
		mog_oom();
	}
}

static void listeners_cleanup(void)
{
	if (!listeners)
		return;
	hash_free(listeners);
	listeners = NULL;
}

static bool listener_close_each(void *_l, void *unused)
{
	struct listener *l = _l;

	syslog(LOG_INFO, "closing unused inherited fd=%d", l->fd);
	mog_close(l->fd);
	l->fd = -1;

	return true;
}

static void listeners_init(void)
{
	if (listeners) return;
	listeners = hash_initialize(3, NULL, listener_hash, listener_cmp, free);
	mog_oom_if_null(listeners);
	atexit(listeners_cleanup);
}

static unsigned long listen_env(const char *env)
{
	const char *e = getenv(env);
	unsigned long tmp;
	char *end;

	if (!e) return ULONG_MAX;
	errno = 0;
	tmp = strtoul(e, &end, 10);
	if (errno) die_errno("failed to parse %s: %s", env, e);
	if (*end) die("trailing byte in %s: %s", env, e);

	return tmp;
}

/* systemd-style socket activation in the vein of sd_listen_fds(3) */
static void systemd_inherit_fds(void)
{
	const int listen_fds_start = 3; /* SD_LISTEN_FDS_START */
	int fd, listen_fds_end;
	unsigned long tmp = listen_env("LISTEN_PID");

	if (getpid() != (pid_t)tmp) goto out;

	tmp = listen_env("LISTEN_FDS");
	if (tmp > INT_MAX) die("LISTEN_FDS out of range: %lu", tmp);

	listeners_init();
	listen_fds_end = listen_fds_start + (int)tmp;
	for (fd = listen_fds_start; fd < listen_fds_end; fd++) {
		if (mog_set_cloexec(fd, true) == 0)
			register_listen_fd(fd);
		else
			die("inherited out %d of %lu LISTEN_FDS",
			    fd - listen_fds_start, tmp);
	}
out:
	unsetenv("LISTEN_FDS");
	unsetenv("LISTEN_PID");
}

/* close all inherited listeners we do not need */
void mog_inherit_cleanup(void)
{
	if (!listeners)
		return;

	hash_do_for_each(listeners, listener_close_each, NULL);
	listeners_cleanup();
}

/* returns the FD belonging to the address if it was inherited */
int mog_inherit_get(struct sockaddr *addr, socklen_t len)
{
	struct listener l;
	struct listener *in;
	int fd = -1;

	if (!listeners)
		return fd;

	l.len = len;
	memcpy(&l.msa.bytes, addr, len);

	in = hash_delete(listeners, &l);
	if (in) {
		fd = in->fd;
		free(in);
		CHECK(int, 0, mog_set_cloexec(fd, true));
	}

	return fd;
}

void mog_inherit_init(void)
{
	char *orig = getenv("CMOGSTORED_FD");
	char *fds;
	char *tip;
	char *end;
	unsigned long fd;
	unsigned endbyte;

	systemd_inherit_fds();

	if (orig == NULL)
		return;

	listeners_init();
	fds = xstrdup(orig);
	tip = fds;

	for (;;) {
		errno = 0;
		fd = strtoul(tip, &end, 10);
		if (errno == 0) {
			if (fd > INT_MAX)
				die("inherited fd=%lu is too large", fd);
			register_listen_fd((int)fd);
		} else {
			die_errno("strtuol failed to parse: %s", tip);
		}

		/* the end (or error) */
		endbyte = *end;
		if (endbyte != ',')
			break;

		tip = end + 1; /* more FDs to parse */
	}

	free(fds);

	if (endbyte != '\0')
		die("CMOGSTORED_FD contained invalid byte: %u", endbyte);
}
