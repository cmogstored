/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#ifdef HAVE_KQUEUE
#include <sys/types.h>
#include <sys/event.h>
enum mog_qev {
	MOG_QEV_RD = EVFILT_READ,
	MOG_QEV_WR = EVFILT_WRITE,
	MOG_QEV_RW = 0
};
struct mog_queue;
struct mog_fd;
#endif /* HAVE_KQUEUE */

#if defined(LIBKQUEUE) && (LIBKQUEUE == 1)
#  define MOG_LIBKQUEUE (true)
#else
#  define MOG_LIBKQUEUE (false)
#endif
