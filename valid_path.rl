/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "cmogstored.h"

/*
 * we could just use strstr(), but it's buggy on some glibc and
 * we can expand this later (to tighten down to non-FIDs, for example)
 */
%%{
	machine path_traversal;
	main := any* ("..") @ { found = true; fbreak; } any*;
}%%

%% write data;

static bool path_traversal_found(const char *buf, size_t len)
{
	const char *p, *pe;
	bool found = false;
	int cs;
	%% write init;

	p = buf;
	pe = buf + len;
	%% write exec;

	return found;
}

int mog_valid_path(const char *buf, size_t len)
{
	if (len >= MOG_PATH_MAX)
		return 0;

	return ! path_traversal_found(buf, len);
}
