/*
 * Copyright (C) 2012-2020 all contributors <cmogstored-public@yhbt.net>
 * License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
 */
#include "cmogstored.h"

struct mog_fd *
mog_accept_init(int fd, struct mog_svc *svc,
		struct mog_addrinfo *a, mog_post_accept_fn fn)
{
	struct mog_fd *mfd = mog_fd_init(fd, MOG_FD_TYPE_ACCEPT);
	struct mog_accept *ac = &mfd->as.accept;

	ac->post_accept_fn = fn;
	ac->svc = svc;
	ac->addrinfo = a;
	memset(&ac->thrpool, 0, sizeof(struct mog_thrpool));

	return mfd;
}
